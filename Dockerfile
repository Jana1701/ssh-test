FROM registry.gitlab.com/jana1701/python:3.8-slim as base

WORKDIR usr/src/app

ADD . .

FROM base AS build
COPY requirements.txt .
RUN pip install --no-cache -r requirements.txt -t ./ --trusted-host pypi.org --trusted-host files.pythonhosted.org
COPY . .